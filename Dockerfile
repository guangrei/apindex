FROM python:3-alpine
MAINTAINER Am K<amek_chesster@yahoo.com.hk>

RUN mkdir -p /usr/share/apindex/template
COPY apindex.py /usr/share/apindex/apindex
COPY template /usr/share/apindex/template
RUN chmod +x /usr/share/apindex/apindex
RUN ln -s /usr/share/apindex/apindex /usr/local/bin/apindex
ENV APINDEX_TEMPLATE_PREFIX="/usr/share/apindex/template/"
RUN mkdir -p /var/www/public_html
VOLUME /var/www/public_html
WORKDIR /var/www/public_html
EXPOSE 80
CMD ["apindex", "."]