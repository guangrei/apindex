[![pipelines](https://gitlab.com/guangrei/apindex/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/guangrei/apindex/-/pipelines)

apindex for gitlab pages based on [jayanta525/apindex](https://github.com/jayanta525/apindex)

Example:

```yaml
image: registry.gitlab.com/guangrei/apindex/image:latest

before_script:
  - apk update
  - apk add rsync

pages:
    stage: deploy
    script:
    - mkdir public
    - rsync -r --exclude-from='ignore.txt' . public/
    - apindex public
    artifacts:
      paths:
       - public
```

modified & dockerized by [guangrei](https://gitlab.com/guangrei).
